clone the repository

create a vhost in your apache config that points to the folder created
create a new database and add a user to it and update .env folder with the database credentials

php bin/console doctrine:schema:update --force
# this should create a table called postcode in the new created database

php bin/console postcode:import
# this should download the postcode file remotely and trigger the import of the postcodes in this file

# given the nature of this task relying on the download of a gigantic file. there is currently only a limited number of tests performed.
# And most of the code was written modifying the function \App\Model\FileDownloader::getFileFromFileUrl to fake the download and instead focus on the data import. 
# If needed, it is possible to tweak this method to fake the download and have the file right way usable for the import
# the csv file to download is in the git repository at var/Data/ONSPD_NOV_2018_UK.csv

http://cvsimport.test/api/postcodes/near/52.2/-1.9
# this url should output some postcodes assuming the import above went ok and no more than 100 results should be returned

http://cvsimport.test/api/postcodes/partialmatch/BA
# this url should output some postcodes and no more than 100 results should be returned

bin/console server:run
# this command should launch graphql server on localhost to perform the queries below

{
    postcodeByPartialMatch(postcodesearch:"AB1", page:1, limit: 50) {
      postcode
      latitude
      longitude
    }
}

{
    findPostcodesNearPosition(latitude:57.1, longitude:-2.23, near:0.1, page: 1, limit: 50) {
      postcode
      latitude
      longitude
    }
}
 
