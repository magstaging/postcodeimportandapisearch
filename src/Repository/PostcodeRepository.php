<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 23/12/2018
 * Time: 21:53
 */

namespace App\Repository;

use App\Entity\Postcode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class PostcodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Postcode::class);
    }

    /**
     * @param $match
     * @param $page
     * @param $limit
     * @return Pagerfanta
     * @throws \Exception
     */
    public function findMatchingPostcode($match, $page, $limit): Pagerfanta
    {
        if ($match == '') {
            throw new \Exception('No postcode search keyword was submitted');
        }

        $qb = $this->createQueryBuilder('p')
            //->addSelect('p.latitude', 'p.longitude')
            ->where('p.postcode like :match')
            ->setParameter('match', "%$match%")
            ->orderBy('p.postcode', 'ASC');

        return $this->createPaginator($qb->getQuery(), $page, $limit);
    }

    private function createPaginator(Query $query, int $page, int $limit): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($limit);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    /**
     * @param [] $position
     * @param float $near
     * @param int $page
     * @param int $limit
     * @return Pagerfanta
     * @throws \Exception
     */
    public function findPostcodesNearLocation($position, $near, $page, $limit)
    {
        if (!isset($position['latitude']) or !isset($position['longitude'])) {
            throw new \Exception('The position that was submitted is invalid');
        }

        $qb = $this->createQueryBuilder('p')
            ->where('((p.latitude - (:latitude)) * (p.latitude - (:latitude))
                        + (p.longitude - (:longitude)) * (p.longitude - :longitude))
                        < (:near * :near)')
            ->setParameter('latitude', $position['latitude'])
            ->setParameter('longitude', $position['longitude'])
            ->setParameter('near', $near)
            ->setMaxResults($limit)
            ->orderBy('p.postcode', 'ASC');

//        $qb = $this->createQueryBuilder('p')
//            ->where(sprintf('((latitude - (%s))*(latitude - (%s))
//                +(longitude - (%s))*(longitude - (%s)))
//                < (%s * %s)',
//                $position['latitude'],
//                $position['latitude'],
//                $position['longitude'],
//                $position['longitude'],
//                $near,
//                $near))
//            ->setParameter('latitude', $position['latitude'])
//            ->setParameter('longitude', $position['longitude'])
//            ->setParameter('near', $near)
//            ->setMaxResults($limit)
//            ->orderBy('p.postcode', 'ASC');

        return $this->createPaginator($qb->getQuery(), $page, $limit);
    }
}