<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 23/12/2018
 * Time: 12:29
 */

namespace App\Model;

use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class FileDownloader
 * @package App\Model
 */
class FileDownloader
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    private static $fakeMethod = false;

    public function __construct(
        KernelInterface $kernel
    ) {
        $this->kernel = $kernel;
    }

    public function getFileFromFileUrl($url)
    {
        //$postcodeFilePath = $this->kernel->getProjectDir() . '/var/';
        //$filename = $this->findCsvFileInDataFolder($postcodeFilePath);
        //return $filename;
        //$filename = $this->getCsvFilenameFromCompressedFileUrl($url);
        $destination = $this->getTemporaryFilename();

        if ($this->getRemoteFile($url, $destination)) {
            // Handle Zip file to extract in your system
            $zip = new \ZipArchive();
            try {
                if($zip->open($destination) == true) {
                    $postcodeFilePath = $this->kernel->getProjectDir() . '/var/';
                    $zip->extractTo($postcodeFilePath);
                    $zip->close();
                    $filename = $this->findCsvFileInDataFolder($postcodeFilePath);
                    //unlink($destination);
                } else {
                    $filename = '';
                }
            } catch (\Exception $e) {
                //unlink($destination);
                $filename = '';
            }
        } else {
            //throwException('The remote file could not be downloaded');
            $filename = '';
        }

        return $filename;
    }

    /**
     * @param $url
     * @return string
     */
    private function getCsvFilenameFromCompressedFileUrl($url):string
    {
        $compressedFilenameFromUrl = basename($url);
        $filename = substr($compressedFilenameFromUrl, 0, strpos($compressedFilenameFromUrl, '.zip'));

        return $filename;
    }

    /**
     * @return bool|string
     */
    private function getTemporaryFilename()
    {
        $destination = tempnam(sys_get_temp_dir(), 'file.');
        //return '/Users/hervetribouilloy/csvImport/var/2018-11.zip';
        return $destination;
    }

    /**
     * @param $url
     * @param $client
     * @param $destination
     * @return mixed
     */
    private function getRemoteFile($url, $destination)
    {
        if (self::$fakeMethod) {
            $destination = '/private/var/folders/2q/_xqmm45n3rx003j_yzfdcpkc0000gn/T/file.lolNiN';
            return $destination;
        } else {
            $client =  new Client();
            try {
                $response = $client->get($url);

                if ($response->getStatusCode() == 200) {
                    $destination = true;
                } else {
                    $destination = false;
                }
            } catch (\GuzzleHttp\Exception\GuzzleException $e) {
                $destination = false;
            }
        }

        return $destination;
    }

    private function findCsvFileInDataFolder($path)
    {
        $dataPath = $path . '/Data';
        if (!is_dir($dataPath)) {
            return false;
        }

        $handle = opendir($dataPath);
        while (false !== ($file = readdir($handle))) {
            $pathParts = pathinfo($file);
            if (isset($pathParts['extension']) and $pathParts['extension'] == 'csv') {
                return $dataPath.'/'.$file;
            }
        }

        return false;
    }
}