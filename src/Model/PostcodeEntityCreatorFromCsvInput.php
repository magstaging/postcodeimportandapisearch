<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 23/12/2018
 * Time: 17:20
 */

namespace App\Model;

use App\Entity\Postcode;
use Doctrine\ORM\EntityManagerInterface;

class PostcodeEntityCreatorFromCsvInput
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private static $batchSize = 100;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function generatePostcodeFromCsvContent($csvContent, $counter)
    {
        return true;
        $postcodeRecord = new Postcode();
        $postcodeRecord->setPostcode($csvContent['postcode']);
        $postcodeRecord->setLatitude($csvContent['latitude']);
        $postcodeRecord->setLongitude($csvContent['longitude']);

        $this->entityManager->persist($postcodeRecord);

        if (($counter % self::$batchSize) === 0) {
            $this->flushAll();
        }
    }

    public function flushAll()
    {
        $this->entityManager->flush();
        //$this->entityManager->clear();
    }

}