<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 23/12/2018
 * Time: 16:53
 */

namespace App\Model;

class CsvFileReader
{
    private static $limitRowToProcess = -1;

    private $rowCounter = 0;

    private $rowHeader;

    private static $keysToRead = ['pcd', 'lat' , 'long'];

    public function getCsvFileContent($filePath)
    {
        $rows = [];
        ini_set('auto_detect_line_endings',true);

        if ($fileHandle = $this->getFileHandle($filePath)) {
            $this->readCsvHeader($fileHandle);

            while (!$this->isRowCounterReachingTheLimitOfRowsToProcess()
                and ($row = $this->getNextRowToProcess($fileHandle))
            ) {
                if ($this->isRowValid($row)) {
                    $rows[] = $this->getRowContent($row);
                }
            }
        }

        return $rows;
    }

    /**
     * @param $handle
     */
    private function getNextRowToProcess($handle)
    {
        if (($row = fgetcsv($handle)) !== false) {
            $this->rowCounter++;
            return $row;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function isRowCounterReachingTheLimitOfRowsToProcess()
    {
        if (!$this->doWeCareToLimitTheNumberOfRowsToProcess()) {
            return false;
        }

        if ($this->rowCounter == self::$limitRowToProcess) {
            return true;
        }

        return false;
    }

    /**
     * @param $filePath
     * @return bool|resource
     */
    private function getFileHandle($filePath)
    {
        $handle = fopen($filePath, "r");
        if ($handle !== false) {
            return $handle;
        }

        return false;
    }

    private function readCsvHeader($fileHandle)
    {
        $this->rowHeader = $this->getNextRowToProcess($fileHandle);
    }

    private function isRowValid($row)
    {
        if (count($row) == count($this->rowHeader)) {
            return true;
        }
    }

    private function getRowContent($row)
    {
        $result = [];
        foreach ($row as $key => $value) {
            if (!$this->keepRecordOfKey($key)) continue;
            $result[$this->getColumnName($key)] = $value;
        }

        return $result;
    }

    private function keepRecordOfKey($key)
    {
        if (in_array($this->rowHeader[$key], self::$keysToRead)) {
            return true;
        }

        return false;
    }

    /**
     * @param $key
     * @return mixed
     */
    private function getColumnName($key)
    {
        switch ($this->rowHeader[$key]) {
            case 'lat':
                return 'latitude';
                break;
            case 'long':
                return 'longitude';
                break;
            case 'pcd':
                return 'postcode';
                break;
            default:
                return $this->rowHeader[$key];
        }
    }

    /**
     * @return bool
     */
    private function doWeCareToLimitTheNumberOfRowsToProcess(): bool
    {
        return self::$limitRowToProcess > 0;
    }
}