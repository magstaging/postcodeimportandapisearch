<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 23/12/2018
 * Time: 11:39
 */

namespace App\Command;

use App\Model\CsvFileReader;
use App\Model\FileDownloader;
use App\Model\PostcodeEntityCreatorFromCsvInput;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AddPostcodeCommand extends Command
{
    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var FileDownloader
     */
    private $fileDownloader;
    /**
     * @var CsvFileReader
     */
    private $csvFileReader;
    /**
     * @var PostcodeEntityCreatorFromCsvInput
     */
    private $postcodeCreatorFromCsvInput;

    private $rowCounter = 0;

    public function __construct(
        FileDownloader $fileDownloader,
        CsvFileReader $csvFileReader,
        PostcodeEntityCreatorFromCsvInput $postcodeCreatorFromCsvInput,
        $name = null
    ) {
        parent::__construct($name);
        $this->fileDownloader = $fileDownloader;
        $this->csvFileReader = $csvFileReader;
        $this->postcodeCreatorFromCsvInput = $postcodeCreatorFromCsvInput;
    }

    protected function configure()
    {
        $this->setDescription('Run nmy first command in symfony')
            ->setName('postcode:import');
    }

    /**
     * This optional method is the first one executed for a command after configure()
     * and is useful to initialize properties based on the input arguments and options.
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->title('Import csv postcode feed');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $csvFilePathWithPostcodes = $this->downloadRemoteFileWithPostcodes();
            $csvContent = $this->getCsvFileContent($csvFilePathWithPostcodes);

            foreach ($csvContent as $rowContent) {
                $this->generatePostcodeRecordFromCsvRowData($rowContent);
            }
            $this->postcodeCreatorFromCsvInput->flushAll();

            $this->io->success('csv postcode was successfully imported');
        } catch (\Exception $e) {
            $this->io->success('csv postcode could not be downloaded');
        }
    }

    /**
     * @return string
     */
    private function getRemoteFileUrl(): string
    {
        //$remotePath = 'http://spatialkeydocs.s3.amazonaws.com/FL_insurance_sample.csv.zip';
        $remotePath = 'http://parlvid.mysociety.org/os/ONSPD/2018-11.zip';
        return $remotePath;
    }

    /**
     * @return string
     */
    private function downloadRemoteFileWithPostcodes(): string
    {
        $csvFileWithPostcodes = $this->fileDownloader->getFileFromFileUrl($this->getRemoteFileUrl());
        $this->io->success('csv postcode file was downloaded');
        return $csvFileWithPostcodes;
    }

    /**
     * @param $csvFilePathWithPostcodes
     * @return array
     */
    private function getCsvFileContent($csvFilePathWithPostcodes): array
    {
        $csvContent = $this->csvFileReader->getCsvFileContent($csvFilePathWithPostcodes);
        $this->io->success('csv postcode was successfully extracted');
        return $csvContent;
    }

    /**
     * @param $postcodeContent
     */
    private function generatePostcodeRecordFromCsvRowData($postcodeContent): void
    {
        $this->postcodeCreatorFromCsvInput->generatePostcodeFromCsvContent($postcodeContent, $this->rowCounter);
        $this->rowCounter++;
        $this->io->comment(sprintf('%s: a new postcode record was created: %s',
            $this->rowCounter,
            $postcodeContent['postcode']
        ));
    }
}