<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 26/12/2018
 * Time: 17:48
 */

namespace App\GraphQL\Resolver;

use App\Entity\Postcode;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use GraphQL\Type\Definition\ResolveInfo;

class PostcodeResolver implements ResolverInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var \App\Repository\PostcodeRepository
     */
    private $postcodeRepository;

    public function __construct(
        EntityManagerInterface $em,
        \App\Repository\PostcodeRepository $postcodeRepository
    ) {
        $this->em = $em;
        $this->postcodeRepository = $postcodeRepository;
    }

    public function __invoke(ResolveInfo $info, $value, Argument $args)
    {
        $method = $info->fieldName;
        return $this->$method($value, $args);
    }

    public function postcode(Postcode $postcode): string
    {
        return $postcode->getPostcode();
    }

    public function latitude(Postcode $postcode): string
    {
        return $postcode->getLatitude();
    }

    public function longitude(Postcode $postcode): string
    {
        return $postcode->getLongitude();
    }

    public function resolve(string $id) :Postcode
    {
        return $this->em->find(Postcode::class, $id);
    }

    public function findById(string $id) :Postcode
    {
        return $this->em->find(Postcode::class, $id);
    }

    public function findMatchingPostcode(string $match, int $page = 1, int $limit = 100)
    {
        return $this->postcodeRepository->findMatchingPostcode($match, $page, $limit);
    }

    public function findPostcodesNearPosition(float $latitude, float $longitude, float $near, int $page = 1, int $limit = 100)
    {
        return $this->postcodeRepository->findPostcodesNearLocation($this->getRequestPosition($latitude, $longitude), $near, $page, $limit);
    }

    private function getRequestPosition($latitude, $longitude)
    {
        return [
            'latitude' => $latitude,
            'longitude' => $longitude
        ];
    }
}