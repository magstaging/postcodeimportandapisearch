<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 23/12/2018
 * Time: 09:18
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    /**
     * @var \App\Repository\PostcodeRepository
     */
    private $postcodeRepository;

    public function __construct(
        \App\Repository\PostcodeRepository $postcodeRepository
    ) {
        $this->postcodeRepository = $postcodeRepository;
    }

    /**
     * @Route("/simplicity")
     */
    public function simple()
    {
        return new Response('Simple! Easy! Great!');
    }

    /**
    * @Route("/hello/{name}")
    */
    public function index($name) {
        return new Response('Simple! Easy! Great!'.$name);
    }

    /**
     * @Route("/api/postcodes/partialmatch/{match}")
     * @return JsonResponse
     */
    public function findPostcodesByPartialMatch(string $match, int $page = 1, int $limit = 100)
    {
        try {
            $postcodes = $this->postcodeRepository->findMatchingPostcode($match, $page, $limit);
        } catch (\Exception $e) {
            return $this->returnExceptionResponse($e);
        }

        if ($postcodes->getNbResults()>0) {
            $response = $this->assignPostcodeDataToResponse($postcodes);
        } else {
            $response = $this->buildErrorResponse(
                sprintf('No postcodes match the string %s', $match)
            );
        }

        return $this->json($response);
    }

    /**
     * @Route("/api/postcodes/near/{latitude}/{longitude}")
     * @return JsonResponse
     */
    public function findPostcodesNearPosition($latitude, $longitude, float $near = 0.1, int $page = 1, int $limit = 100)
    {
        try {
            $postcodes = $this->postcodeRepository->findPostcodesNearLocation($this->getRequestPosition($latitude, $longitude), $near, $page, $limit);
        } catch (\Exception $e) {
            return $this->returnExceptionResponse($e);
        }

        if ($postcodes->getNbResults()>0) {
            $response = $this->assignPostcodeDataToResponse($postcodes);
        } else {
            $response = $this->buildErrorResponse(
                sprintf('No postcodes match the location latitude: %s, longitude: %s',
                    $latitude,
                    $longitude
                )
            );
        }

        return $this->json($response);
    }

    private function getRequestPosition($latitude, $longitude)
    {
        return [
            'latitude' => $latitude,
            'longitude' => $longitude
        ];
    }

    /**
     * @param \Exception $e
     * @return JsonResponse
     */
    private function returnExceptionResponse(\Exception $e): JsonResponse
    {
        $response[] = [
            'error' => true,
            'message' => $e->getMessage()
        ];

        return $this->json($response);
    }

    /**
     * @param $postcodes
     * @return array
     */
    private function assignPostcodeDataToResponse($postcodes): array
    {
        $response = [];

        /** @var \App\Entity\Postcode $postcode */
        foreach ($postcodes as $postcode) {
            $response[] = [
                'postcode' => $postcode->getPostcode(),
                'latitude' => $postcode->getLatitude(),
                'longitude' => $postcode->getLongitude()
            ];
        }
        return $response;
    }

    /**
     * @param $message
     * @return array
     */
    private function buildErrorResponse($message): array
    {
        $response[] = [
            'error' => true,
            'message' => $message
        ];
        return $response;
    }
}